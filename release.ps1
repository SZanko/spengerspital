mkdir -p spengerspital-zanko
# Weil Windows Powershell COPY-ITEM es nicht schafft mehrere Files auf einmal zu kopieren schreibe ich jetzt für jede einzelne Zeile copy
# cp -R LICENSE Makefile README.md build.gradle.kts settings.gradle.kts config.mk src/ spengerspital-zanko
cp build.gradle.kts spengerspital-zanko
cp settings.gradle.kts spengerspital-zanko
cp LICENSE spengerspital-zanko
cp Makefile spengerspital-zanko
cp README.md spengerspital-zanko
cp config.mk spengerspital-zanko
cp src/ spengerspital-zanko
Compress-Archive -Path spengerspital-zanko -DestinationPath spengerspital-zanko.zip
