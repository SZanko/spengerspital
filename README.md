# Spengerspital

A Spring Boot Application which tries to implement the [FHIR Standard](https://www.hl7.org/fhir/)

## Dependencies

### Build Dependencies

- Java Version = 11
- Gradle Version = 6.6.1

### Runtime Dependencies

- MariaDB Server

### Artix | Arch GNU/Linux

```` sudo pacman -S gradle jdk11-openjdk ````

### Podman/Docker

```` podman pull gradle:jdk11 ````

## How to run it

### Artix | Arch GNU/Linux

```` gradle bootRun ````

### Podman/Docker

```` podman run --rm -u gradle -v "$PWD":/home/gradle/project -w /home/gradle/project gradle:jdk11 bootRun ````

## How to build it

### Artix | Arch GNU/Linux

```` gradle bootJar ````
