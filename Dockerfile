# Use the specified base image
FROM eclipse-temurin:11.0.21_9-jdk-jammy

# Disable the Gradle daemon for CI servers
ENV GRADLE_OPTS="-Dorg.gradle.daemon=false"

# Set the working directory
WORKDIR /app

# Copy the entire project
COPY . .

# Build the application
RUN ./gradlew --build-cache assemble

# Expose the port used by your application (adjust if needed)
EXPOSE 8080

# Command to run the application
CMD ["java", "-jar", "build/libs/Spengerspital-0.0.1-SNAPSHOT.jar"]
