import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
    id("org.springframework.boot") version "2.3.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    java
    application
    war
    id("io.freefair.lombok") version "5.0.0-rc4"
    id("checkstyle")
}

group = "at.zanko.spengerspital"
version = "v1.1"

allprojects {
    repositories {
        mavenCentral()
        maven {
            url = uri("https://repo.spring.io/milestone")
        }
        maven {
            url = uri("https://gitlab.com/api/v4/projects/18196841/packages/maven")
            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }

    java.sourceCompatibility = JavaVersion.VERSION_11

    checkstyle.toolVersion = "10.3.3"
    checkstyle.configFile = rootProject.file("config/checkstyle/checkstyle.xml")
    checkstyle.sourceSets = listOf()

    tasks.withType<Checkstyle>().configureEach {
        isShowViolations = true

        reports {
            xml.required.set(false)
            html.required.set(true)
        }
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.apache.commons:commons-collections4:4.4")

    runtimeOnly("org.springframework.boot:spring-boot-devtools")
    implementation("org.mariadb.jdbc:mariadb-java-client")//MariaDB Connector
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("junit:junit")

    implementation("com.h2database:h2")
    compileOnly("org.projectlombok:lombok") //Add Lombok Support for Project
    annotationProcessor("org.projectlombok:lombok")
    testCompileOnly("org.projectlombok:lombok")
    testAnnotationProcessor("org.projectlombok:lombok")
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        showExceptions = true
        showStandardStreams = true
        events(PASSED, SKIPPED, FAILED)
    }
}