# spengerspital
# See LICENSE file for copyright and license details.

include config.mk

all: options spengerspital

wrapper:
	curl -O https://raw.githubusercontent.com/int128/latest-gradle-wrapper/master/gradlew -O https://raw.githubusercontent.com/int128/latest-gradle-wrapper/master/gradlew.bat
	chmod +x ./gradlew ./gradlew.bat
	mkdir -p ./gradle/wrapper
	curl -O https://raw.githubusercontent.com/int128/latest-gradle-wrapper/master/gradle/wrapper/gradle-wrapper.jar -O https://raw.githubusercontent.com/int128/latest-gradle-wrapper/master/gradle/wrapper/gradle-wrapper.properties
	mv ./gradle-wrapper.properties ./gradle-wrapper.jar ./gradle/wrapper/

tags:
	ctags -R *

test: clean, tags
	./gradlew test | ./gradlew.bat test

build-container: clean build-Jar
	docker build -t szanko/spengerspital .

run-container: build-container
	docker run -p 8080:8080 szanko/spengerspital

build-Jar: clean wrapper
	./gradlew bootJar

run: clean, tags
	./gradlew bootRun

clean:
	rm -rf ./bin ./build/ ./.classpath ./.gradle/ .idea/ .settings/ ./.project  ./gradle/  ./gradlew ./gradlew.bat tags

dist: clean
	mkdir -p spengerspital-${VERSION}
	cp -R LICENSE Makefile README.md config.mk\
		src/ spengerspital-${VERSION}
	tar -cf spengerspital-${VERSION}.tar spengerspital-${VERSION}
	gzip spengerspital-${VERSION}.tar
	rm -rf spengerspital-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f spengerspital ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/spengerspital
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < spengerspital.1 > ${DESTDIR}${MANPREFIX}/man1/spengerspital.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/spengerspital.1
	mkdir -p ${DESTDIR}${PREFIX}/share/spengerspital
	cp -f larbs.mom ${DESTDIR}${PREFIX}/share/spengerspital
	chmod 644 ${DESTDIR}${PREFIX}/share/spengerspital/larbs.mom

springbootUser:
	sudo useradd spengerspital
	sudo passwd spengerspital
	./gradlew bootJar 
	sudo chown spengerspital:spengerspital ./build/libs/Spengerspital-${VERSION}.jar
	sudo chmod 500 ./build/libs/Spengerspital-${VERSION}.jar

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/spengerspital\
		${DESTDIR}${PREFIX}/share/spengerspital/larbs.mom\
		${DESTDIR}${MANPREFIX}/man1/spengerspital.1

.PHONY: echo all  clean dist install test uninstall wrapper 
