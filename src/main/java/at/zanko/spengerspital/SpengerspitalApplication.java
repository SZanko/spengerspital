package at.zanko.spengerspital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class SpengerspitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpengerspitalApplication.class, args);
	}

}
