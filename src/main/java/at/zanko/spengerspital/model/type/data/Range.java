package at.zanko.spengerspital.model.type.data;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

/**
 * <a href =  https://www.hl7.org/fhir/datatypes.html#Range> Range Datatype</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10
 */

@Embeddable
public @Data
class Range  {

    /**
     * Low limit
     * @return SimpleQuantity from Low Limit
     */
    @OneToOne
    private Quantity low;

    public void setLow(Quantity low) {
        if(low.getComparator()==null)
        {
            this.low= low;
        }
        else {
            throw new IllegalArgumentException("This is a simple Quantity comparator has not to be set");
        }
    }

    /**
     * High limit
     * @return SimpleQuantity from Low Limit
     */
    @OneToOne
    private Quantity high;

    public void setHigh(Quantity high) {
        if(high.getComparator()==null)
        {
            this.high = high;
        }
        else {
            throw new IllegalArgumentException("This is a simple Quantity comparator has not to be set");
        }
    }
}
