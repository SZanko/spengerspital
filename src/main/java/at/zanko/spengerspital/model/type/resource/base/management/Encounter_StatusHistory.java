package at.zanko.spengerspital.model.type.resource.base.management;

import javax.persistence.*;
import java.util.Set;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Period;
import lombok.*;

@Entity
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data
class Encounter_StatusHistory extends BackboneElement {


    public enum StatusCode{
        planned , arrived , triaged , inprogress , onleave , finished ,cancelled
    }
    @Enumerated(EnumType.STRING)
    private StatusCode status;

    @Embedded
    private Period period;
}
