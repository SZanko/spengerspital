package at.zanko.spengerspital.model.type.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#ContactPoint> ContactPoint DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public  @Data
class ContactPoint extends Element {

    public enum SystemCode{
        phone,fax,email,pager,url,sms,other
    }
    public enum UseCode{
        home,work,temp,old,mobile
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "ctp_system")
    private SystemCode system;

    @Column(name="ctp_value")
    private String value;

    @Enumerated(EnumType.STRING)
    @Column(name="ctp_use")
    private UseCode use;

    @Column(name="ctp_rank")
    private int rank;

    @Column(name="ctp_period")
    private Period period;
}
