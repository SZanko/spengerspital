package at.zanko.spengerspital.model.type.data;

import java.net.URI;

import javax.persistence.*;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

/**
 * <a href=https://hl7.org/FHIR/datatypes.html#Reference> Reference </a>
 *
 * @author	Stefan Zanko
 * @version 1.1
 * @since	2020-02-01
 */

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="rf_reference")
public @Data
class Reference extends Element {

    @Column(name = "rf_reference")
    private String reference;

    @Column(name="rf_type")
    private String type;

    @OneToOne
    @JoinColumn(name = "rf_identifier")
    private Identifier identifier;

    @Column(name = "rf_display")
    private String display;
}
