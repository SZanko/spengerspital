package at.zanko.spengerspital.model.type.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public @Data
class Resource{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private String id;
}
