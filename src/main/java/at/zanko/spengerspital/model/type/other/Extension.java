package at.zanko.spengerspital.model.type.other;

import javax.persistence.Column;
import javax.persistence.Entity;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
@NoArgsConstructor
public @Data
class Extension extends Element {

    @Column(name="exten_url")
    private String url;

    //Muss in der Application als String deklariert werden
    @Column(name="exten_value")
    private String value;

}
