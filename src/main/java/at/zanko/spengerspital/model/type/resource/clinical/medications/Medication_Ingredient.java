package at.zanko.spengerspital.model.type.resource.clinical.medications;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Ratio;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-09-21
 */

@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Medication_Ingredient extends BackboneElement {

	public void setItemReference(Reference itemReference) {
		if(getItemReference() == null) {
			this.itemReference = itemReference;
		} else {
			throw new IllegalArgumentException("itemReference and itemCodeableConcept cannot be set at the same time");
		}
	}

	public void setItemCodeableConcept(CodeableConcept itemCodeableConcept) {
		if(getItemReference() == null) {
			this.itemCodeableConcept = itemCodeableConcept;
		} else {
			throw new IllegalArgumentException("itemCodeableConcept and itemReference cannot be set at the same time");
		}
	}

	public Medication_Ingredient(CodeableConcept itemCodeableConcept, Boolean isactive, Ratio strength) {
		this.itemReference = itemReference;
		this.isActive = isactive;
		this.strength = strength;
	}
	public Medication_Ingredient(Reference itemReference, Boolean isactive, Ratio strength) {
		this.itemReference = itemReference;
		this.isActive = isactive;
		this.strength = strength;
	}

	@OneToOne
	private Reference itemReference;

	@OneToOne
	private CodeableConcept itemCodeableConcept;



	private boolean isActive;
	@OneToOne
	private Ratio strength;
}
