package at.zanko.spengerspital.model.type.resource.clinical.care_provision;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.Column;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Period;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Embeddable
public @Data
class CareTeam_Participant extends BackboneElement {
    @OneToMany(cascade=CascadeType.ALL,targetEntity= CodeableConcept.class,fetch=FetchType.EAGER)
    @JoinColumn(name="participant_careteam_role",referencedColumnName="id")
    private Set<CodeableConcept> role=new HashSet<>();

    @Column(name="participant_careteam_member")
    private Reference member;//Practioner, PractionerRole, RelatedPerson,Patient,Organization,CareTeam

    @Column(name="participant_careteam_onBehalfOf")
    private Reference onBehalfOf;//Organization

    @Column(name="participant_careteam_period")
    private Period period;
}
