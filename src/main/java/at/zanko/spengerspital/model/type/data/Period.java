package at.zanko.spengerspital.model.type.data;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Embeddable
//@Table(name = "pe_period")
@EqualsAndHashCode
public @Data
class Period {

    @Column(name="pe_start")
    private LocalDateTime start;

    @Column(name="pe_end")
    private LocalDateTime end;

}
