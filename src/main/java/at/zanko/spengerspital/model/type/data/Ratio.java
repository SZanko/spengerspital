package at.zanko.spengerspital.model.type.data;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *  <a href = https://www.hl7.org/fhir/datatypes.html#Ratio> Ratio DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-09-16
 */

@NoArgsConstructor
@Entity
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data
class Ratio extends Element{

	@OneToOne
	private Quantity numerity;

	@OneToOne
	private Quantity denominator;

}
