package at.zanko.spengerspital.model.type.data;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.LocalDateTime;

/**
 * <a href = https://www.hl7.org/fhir/deviceusestatement-definitions.html#DeviceUseStatement> Device Use Statment</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-11-11
 */
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class DeviceUseStatement_Timing {
    @Embedded
    private Period timing_period;


    @Column(name="timing_timingDateTime")
    private LocalDateTime timingDateTime;
}
