package at.zanko.spengerspital.model.type.resource.base.individuals;

import javax.persistence.Embedded;
import javax.persistence.OneToOne;
import javax.persistence.Column;
import javax.persistence.Entity;

import at.zanko.spengerspital.model.type.data.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Group_Characteristic extends BackboneElement {

    @OneToOne
    private CodeableConcept code;

    @OneToOne
    private CodeableConcept valueCodeableConcept;

    @Column
    private boolean valueboolean;

    @OneToOne
    private Quantity valueQuantity;

    @Embedded
    private Range valueRange;

    @OneToOne
    private Reference valueReference;

    @Column
    private boolean exclude;

    @Column
    private Period period;
}
