package at.zanko.spengerspital.model.type.data;

import java.net.URI;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

/**
 * <a href=https://hl7.org/FHIR/datatypes.html#Quantity> Quantity </a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-09-12
 */

@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
public @Data
class Quantity extends Element{

    @Column
	private float value;
	public enum ComparatorCode
	{
		greater("<"),
		greaterEqual("<="),
		smallerEqual(">="),
		smaller(">");
		private String value;
		private ComparatorCode(String value)
		{
			this.value=value;
		}
		public String toString()
		{
			return this.value;
		}
	}

	@Enumerated(EnumType.STRING)
	@Column
	private ComparatorCode comparator;

	@Column
	private String unit;

	@Column
	private String system;

}
