package at.zanko.spengerspital.model.type.data;

import at.zanko.spengerspital.model.type.other.DomainResource;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * <a href = https://www.hl7.org/fhir/deviceusestatement-definitions.html#DeviceUseStatement> Device Use Statment</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-11-11
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
public @Data
class DeviceUseStatement extends DomainResource {

    @OneToMany(cascade= CascadeType.ALL,targetEntity= Identifier.class,fetch= FetchType.EAGER)
    @JoinColumn(name="epiofca_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();


    @OneToMany(cascade= CascadeType.ALL,targetEntity= Identifier.class,fetch= FetchType.EAGER)
    @JoinColumn(name="epiofca_identifier",referencedColumnName="id")
    private Set<Reference> basedOn=new HashSet<>();


    public enum DeviceUseStatement_Status{
        //stopped("stopped"),
        completed("completed"),
        active("active"),
        //onhold("onhold"),
        //finished("finished"),
        entered_in_error("entered-in-error");

        private String value;
        private DeviceUseStatement_Status(String value){
            this.value=value;
        }
        public String toString(){
            return this.value;
        }
    }

    @Enumerated(EnumType.STRING)
    @NotNull
    private DeviceUseStatement_Status deviceUseStatement_status;


    @Embedded
    private DeviceUseStatement_Timing timing;


    @Column(name="DevicesUseStatement_recordedOn")
    private LocalDateTime recordedOn;

}
