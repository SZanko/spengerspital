package at.zanko.spengerspital.model.type.other;


import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public @Data class DomainResource extends Resource {

    //@Column(name="do_text")
    //private String text;

    //@OneToMany(cascade = CascadeType.ALL,targetEntity = Resource.class,fetch = FetchType.EAGER)
    //@JoinColumn(name="do_containment",referencedColumnName = "id")
    //private Set<Resource> containment=new HashSet<>();

}
