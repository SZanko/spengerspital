package at.zanko.spengerspital.model.type.resource;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Period;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Embeddable
public @Data
class participant extends BackboneElement {


    @OneToMany(cascade= CascadeType.ALL,targetEntity = CodeableConcept.class,fetch = FetchType.EAGER)
    @JoinColumn(name="part_type",referencedColumnName="id")
    private Set<CodeableConcept> type=new HashSet<>();

    @Column(name="part_type")
    private Period period;

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Reference.class,fetch=FetchType.EAGER)
    @JoinColumn(name="part_episiodeOfCare")
    private Set<Reference> episodeOfCare=new HashSet<>();
}
