package at.zanko.spengerspital.model.type.data;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#HumanName> Coding DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public @Data
class HumanName extends Element {

    public enum UseCode{
        usual,
        official,
        temp,
        nickname,
        anonymous,
        old,
        maiden
        //NameUse (Required)
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "hun_use")
    private UseCode use;

    @Column(name = "hun_text")
    private String text;

    @Column(name = "hun_family")
    private String family;

    @ElementCollection
    @CollectionTable(name = "hun_given", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "hun_given")
    private Set<String> given;


    @ElementCollection
    @CollectionTable(name="hun_prefix",joinColumns = @JoinColumn(name="id"))
    @Column(name="hun_prefix")
    private Set<String> prefix;

    @ElementCollection
    @CollectionTable(name="hun_suffix",joinColumns = @JoinColumn(name="id"))
    @Column(name="hun_suffix")
    private Set<String> suffix;

    @Column(name="hun_period")
    private Period period;

}
