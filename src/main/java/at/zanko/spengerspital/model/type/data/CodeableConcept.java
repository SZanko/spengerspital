package at.zanko.spengerspital.model.type.data;

import javax.persistence.*;
import java.util.Set;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#CodeableConcept> CodeableConcept DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
public @Data
class CodeableConcept extends Element {

    @OneToMany(cascade= CascadeType.ALL,targetEntity = Coding.class,fetch = FetchType.EAGER)
    @JoinColumn(name="c_codeableconcept_fk",referencedColumnName = "id")
    private Set<Coding> coding;

    @Column(name="cc_text")
    private String text;
}
