package at.zanko.spengerspital.model.type.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.net.URL;
import java.time.LocalDateTime;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;
import org.springframework.util.MimeType;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#Attachment> Attachment  DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
public @Data
class Attachment extends Element {


    @Column(name = "atta_contentType")
    private MimeType contentTypeCode;

    public enum LanguageCode{
        ar,bn,cs,da,de
    }

    @Enumerated(EnumType.STRING)
    @Column(name="atta_language")
    private LanguageCode language;

    @Column(name="atta_data")
    private String data;

    @Column(name = "atta_url")
    private URL url;

    @Column(name = "atta_size")
    private int size;

    @Column(name = "atta_hash")
    private String hash;

    @Column(name = "atta_title")
    private String title;

    @Column(name="atta_creation")
    private LocalDateTime creation;
}
