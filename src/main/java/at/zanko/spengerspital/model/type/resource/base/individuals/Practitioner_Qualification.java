package at.zanko.spengerspital.model.type.resource.base.individuals;

import javax.persistence.*;

import at.zanko.spengerspital.model.type.data.*;
import lombok.*;

import java.util.Set;
import java.util.HashSet;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
public @Data
class Practitioner_Qualification extends BackboneElement {
    @OneToMany(cascade= CascadeType.ALL,targetEntity = Identifier.class,fetch = FetchType.EAGER)
    @JoinColumn(name="quali_identifier",referencedColumnName = "id")
    private Set<Identifier> identifier=new HashSet<>();

    @OneToOne
    private CodeableConcept code=new CodeableConcept();

    @Column(name="quali_period")
    private Period period=new Period();

}
