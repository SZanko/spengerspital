package at.zanko.spengerspital.model.type.resource.base.individuals;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
public @Data
class Practitioner_Role_availableTime extends BackboneElement {

    public enum DaysOfWeek{
        mon,
        tue,
        wed,
        thu,
        fri,
        sat,
        sun
        //NameUse (Required)
    }

    @ElementCollection
    @CollectionTable(name="prac_role_daysofweek", joinColumns = @JoinColumn(name="id"))
    @Column(name="prac_role_daysofweek")
    private Set<DaysOfWeek> daysOfWeek;

    @Column
    private boolean allDay;

    @Column
    private LocalDateTime availableStartTime;

    @Column
    private LocalDateTime availableEndTime;
}
