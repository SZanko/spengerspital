package at.zanko.spengerspital.model.type.resource.finical.general;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.OneToOne;
import javax.persistence.Column;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.Period;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/account-definitions.html#Account.guarantor> Account Guarantor</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@Embeddable
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public @Data
class Account_Guarantor extends BackboneElement {
    @OneToOne
    private Reference party;

    @Column
    private boolean onHold;

    @Embedded
    private Period period;
}
