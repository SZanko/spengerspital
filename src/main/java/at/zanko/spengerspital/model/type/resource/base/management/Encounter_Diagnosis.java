package at.zanko.spengerspital.model.type.resource.base.management;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.*;

/**
 * <a href=https://hl7.org/FHIR/datatypes.html#Diagnosis> Diagnosis </a>
 *
 * @author	Stefan Zanko
 * @version 1.1
 * @since	2020-02-01
 */


@Entity
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public @Data
class Encounter_Diagnosis extends BackboneElement {

    @OneToOne
    private Reference condition; //Condition|Procedure

    @OneToOne
    private CodeableConcept use;

    @Column
    private int rank;//positive


    public void setrank(int positivRank) {
        if(positivRank>=0){
            this.rank= positivRank;
        }
        else {
            throw new IllegalArgumentException("rank has to be positiv");
        }
    }
}
