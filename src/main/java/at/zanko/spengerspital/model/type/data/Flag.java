package at.zanko.spengerspital.model.type.data;

import at.zanko.spengerspital.model.type.other.DomainResource;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public @Data
class Flag extends DomainResource {

    @OneToMany(cascade= CascadeType.MERGE,targetEntity = Identifier.class,fetch = FetchType.EAGER)
    @JoinColumn(name="flag_identifier",referencedColumnName = "id")
    private Set<Identifier> identifier;

    public enum FlagStatus {
        activity("activity"),
        inactivity("inactivity"),
        @JsonProperty("entered+end")
        enteredplusend("entered+end");
        private String value;
        FlagStatus(String value) {
            this.value=value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }

    @NonNull
    @Enumerated(EnumType.STRING)
    @Column
    private FlagStatus status;

    @Column
    private Period period;

    @OneToOne(cascade = CascadeType.ALL)
    private Reference author;
}
