package at.zanko.spengerspital.model.type.data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#Address> Address
 * Datatype</a>
 *
 * @author Stefan Zanko
 * @version 1.0
 * @since 2020-04-10
 */

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data class Address extends Element {

	/**
	 * Usecodes that can be used
	 */
	private enum UseCode{
		/**
		 * home
		 */
		home,
		/**
		 * work
		 */
		work,
		/**
		 * temp
		 */
		temp,
		/**
		 * old
		 */
		old,
		/**
		 * billing
		 */
		billing
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "add_use")
	private UseCode use;

	/**
	 * TypeCodes that can be used
	 */
	private enum TypeCode {
		/**
		 * postal
		 */
		postal,
		/**
		 * physical
		 */
		physical,
		/**
		 * both
		 */
		both
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "add_type")
	private TypeCode type;

	@Column(name = "add_text")
	private String text;

	@ElementCollection
	@CollectionTable(name = "add_line", joinColumns = @JoinColumn(name = "id"))
	@Column(name = "add_line")
	private Set<String> line = new HashSet<>();

	@Column(name = "add_city")
	private String city;

	@Column(name = "add_state")
	private String state;

	@Column(name = "add_postalCode")
	private String postalCode;

	@Column(name = "add_country")
	private String country;

	@Column(name = "add_period")
	private Period period;
}
