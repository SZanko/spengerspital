package at.zanko.spengerspital.model.type.data;

import javax.persistence.Column;
import javax.persistence.Entity;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#Coding> Coding DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
public @Data
class Coding extends Element {

    @Column(name="c_system")
    private String system;

    @Column(name="c_code")
    private String code;

    @Column(name="c_version")
    private String version;

    @Column(name="c_display")
    private String display;

    @Column(name="c_userSelected")
    private boolean userSelected;
}
