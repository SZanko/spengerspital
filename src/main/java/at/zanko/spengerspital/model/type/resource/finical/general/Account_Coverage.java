package at.zanko.spengerspital.model.type.resource.finical.general;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.*;

@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Embeddable
public @Data
class Account_Coverage extends BackboneElement {

    @OneToOne
    private Reference coverage;

    public void setPositivInt(int positivInt) {
        if(positivInt>=0){
            this.positivInt = positivInt;
        }
        else {
            throw new IllegalArgumentException("positivInt has to be positiv");
        }
    }

    @Column(name="cover_priority")
    private int positivInt;
}
