package at.zanko.spengerspital.model.type.resource.clinical.medications;

import at.zanko.spengerspital.model.type.data.BackboneElement;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-09-21
 */

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
public @Data
class Medication_Batch extends BackboneElement{

	public Medication_Batch(){
		super();
	}

	@Column
	private String lotNumber;

	@Column
	private LocalDateTime expirationDate;

}
