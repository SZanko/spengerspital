package at.zanko.spengerspital.model.type.data;

import java.util.HashSet;
import java.util.Set;

import at.zanko.spengerspital.model.type.other.Extension;
import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;

/**
 * <a href = https://www.hl7.org/fhir/backboneelement.html> Coding DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public @Data
class BackboneElement extends Element {

    @OneToMany(cascade= CascadeType.ALL,targetEntity = Extension.class,fetch = FetchType.EAGER)
    @JoinColumn(name="backbone_modifierextension",referencedColumnName = "id")
    private Set<Extension> modifierExtension=new HashSet<>();

}
