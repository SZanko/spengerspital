package at.zanko.spengerspital.model.type.data;

import javax.persistence.*;

import at.zanko.spengerspital.model.type.primitive.Element;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/datatypes.html#Identifier> Identifier DataType</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data
class Identifier extends Element {

    public Identifier() {
        super();
    }

    public Identifier(UseCode code, CodeableConcept type, String system, String value, Period period) {
        super();
        this.code = code;
        this.type = type;
        this.system = system;
        this.value = value;
        this.period = period;
    }

    public enum UseCode{
        usual,offical,temp,secondary,old
    }

    @Enumerated(EnumType.STRING)
	@JoinColumn(name="i_code")
    private UseCode code;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "i_codeableconcept_fk",referencedColumnName = "id")
    private CodeableConcept type;

    @Column(name="i_system")
    private String system;

    @Column(name="i_value")
    private String value;

    @Embedded
    private Period period;
}
