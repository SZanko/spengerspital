package at.zanko.spengerspital.model.resource.base.individuals;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import at.zanko.spengerspital.model.type.resource.base.individuals.Group_Characteristic;
import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Reference;

import at.zanko.spengerspital.model.type.other.DomainResource;
import jdk.jfr.Unsigned;

import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/group.html> Group Resource</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="gr_group")
public @Data
class Group extends DomainResource {

    @OneToMany(cascade= CascadeType.ALL,targetEntity = Identifier.class,fetch = FetchType.EAGER)
    @JoinColumn(name="grp_identifier",referencedColumnName = "id")
    private Set<Identifier> identifier=new HashSet<>();

    @Column(name="grp_active")
    private boolean active;

    public enum TypeCode{
        	person , animal , practitioner , device , medication , substance
    }

    @Enumerated(EnumType.STRING)
    @Column(name="grp_type")
    private TypeCode type;

    @Column(name="grp_actual")
    private boolean actual;

    @Column(name="grp_name")
    private String name;

    @OneToOne
    private CodeableConcept code;

    @Column(name="grp_quantity")
    @Unsigned
    private int quantity;

    @OneToOne
    private Reference managingEntity; //Organization| RelatedPerson | Practitioner | PractitionerRole

    @OneToMany(cascade= CascadeType.ALL,targetEntity = Group_Characteristic.class,fetch = FetchType.EAGER)
    @JoinColumn(name="grp_identifier",referencedColumnName = "id")
    private Set<Group_Characteristic> characteristic=new HashSet<>();
}
