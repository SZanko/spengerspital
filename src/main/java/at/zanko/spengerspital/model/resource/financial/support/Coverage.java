package at.zanko.spengerspital.model.resource.financial.support;

import javax.persistence.*;

import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.other.DomainResource;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

/**
 * <a href= https://www.hl7.org/fhir/coverage.html> Coverage Resource</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10	
 */

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Coverage extends DomainResource {

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Identifier.class,fetch=FetchType.EAGER)
    @JoinColumn(name="cover_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();

    public enum StatusCode{
        active("active"),
        cancelled("inactive"),
        draft("draft"),
        entered_in_error("entered_in_error");

        private String value;

        StatusCode(String value) {
            this.value=value;
        }
        public String toString(){

            return this.value;
        }
    }

    @Enumerated(EnumType.STRING)
    private StatusCode status;

    @OneToOne
    @JoinColumn(name="cover_codeconcept")
    private CodeableConcept type;

    //@Column(name="cover_policyholder")
    //private Reference policyholder;

    //@Column(name="cover_subscriber")
    //private Reference subscriber;

    @Column(name="cover_subscriberid")
    private String subscriberid;

    //@Column(name="cover_beneficiary")
    //private Reference beneficiary;

    @Column(name="cover_dependent")
    private String dependent;

    @OneToOne
    @JoinColumn(name="cover_relationship")
    private CodeableConcept relationship;
}
