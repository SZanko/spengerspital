package at.zanko.spengerspital.model.resource.base.individuals;


import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.model.type.other.DomainResource;
import at.zanko.spengerspital.model.type.resource.base.individuals.Practitioner_Role_availableTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public @Data
class PractitionerRole extends DomainResource {
    @OneToMany(cascade= CascadeType.ALL,targetEntity= Identifier.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();

    @Column
    public boolean active;
    
    @Embedded
    public Period period;

    @OneToOne
    @JoinColumn(name="prac_role_prac")
    private Reference practitioner;

    @OneToOne
    @JoinColumn(name="prac_role_organization")
    private Reference organization;

    @OneToMany(cascade= CascadeType.MERGE,targetEntity= CodeableConcept.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_code",referencedColumnName="id")
    private Set<CodeableConcept> code=new HashSet<>();

    @OneToMany(cascade= CascadeType.ALL,targetEntity= CodeableConcept.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_specialty",referencedColumnName="id")
    private Set<CodeableConcept> specialty=new HashSet<>();

    @OneToMany(cascade= CascadeType.ALL,targetEntity= Reference.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_location",referencedColumnName="id")
    private Set<Reference> location=new HashSet<>();

    @OneToMany(cascade= CascadeType.ALL,targetEntity= Reference.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_healthcareservice",referencedColumnName="id")
    private Set<Reference> healthcareService=new HashSet<>();

    @OneToMany(cascade= CascadeType.MERGE,targetEntity= ContactPoint.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_telecom",referencedColumnName="id")
    private Set<ContactPoint> telecom=new HashSet<>();

    @OneToMany(cascade= CascadeType.ALL,targetEntity= Practitioner_Role_availableTime.class,fetch= FetchType.EAGER)
    @JoinColumn(name="prac_role_telecom",referencedColumnName="id")
    private  Set<Practitioner_Role_availableTime> availableTime=new HashSet<>();


}
