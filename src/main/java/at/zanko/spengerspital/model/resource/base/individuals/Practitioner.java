package at.zanko.spengerspital.model.resource.base.individuals;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import at.zanko.spengerspital.model.type.other.DomainResource;
import com.fasterxml.jackson.annotation.JsonProperty;

import at.zanko.spengerspital.model.type.resource.base.individuals.Practitioner_Qualification;
import at.zanko.spengerspital.model.type.data.*;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/practitioner.html> Practitioner Resource</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10
 */

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Practitioner extends DomainResource {

    @OneToMany(cascade=CascadeType.ALL,targetEntity=Identifier.class,fetch=FetchType.EAGER)
    @JoinColumn(name="prac_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();

    @Column(name="prac_inactive")
    private boolean active;

    public enum GenderCode{
        male,female,other,unknown
    }
    @OneToMany(cascade = CascadeType.ALL, targetEntity = HumanName.class, fetch=FetchType.EAGER)
    @JoinColumn(name = "prac_name_fk", referencedColumnName="id")
    private Set<HumanName> name = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, targetEntity = ContactPoint.class, fetch=FetchType.EAGER)
    @JoinColumn(name= "ctp_prac_fk", referencedColumnName = "id")
    private Set<ContactPoint> telecom = new HashSet<>() ;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Address.class, fetch=FetchType.EAGER)
    @JoinColumn(name= "prac_address_fk", referencedColumnName = "id")
    private Set<Address> address = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name="prac_gender")
    private GenderCode gender=GenderCode.unknown;

    @Column(name="prac_birthdate")
    private LocalDate birthDate;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Attachment.class, fetch=FetchType.EAGER)
    @JoinColumn(name= "prac_photo", referencedColumnName = "id")
    private Set<Attachment> photo = new HashSet<>();

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Practitioner_Qualification.class,fetch=FetchType.EAGER)
    @JoinColumn(name="prac_qualification",referencedColumnName="id")
	@JsonProperty("qualification")
    private Set<Practitioner_Qualification> practitionerQualification = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, targetEntity = CodeableConcept.class, fetch=FetchType.EAGER)
    @JoinColumn(name= "prac_communication", referencedColumnName = "id")
    private Set<CodeableConcept> communication = new HashSet<>();

}
