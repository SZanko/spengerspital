package at.zanko.spengerspital.model.resource.base.management;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import at.zanko.spengerspital.model.type.other.DomainResource;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.model.type.resource.base.management.Encounter_Diagnosis;
import at.zanko.spengerspital.model.type.resource.base.management.Encounter_StatusHistory;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/encounter.html> Encounter Resource</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10
 */

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Encounter extends DomainResource{
    public Encounter(Set<Identifier> identifier,
                     StatusCode status,
                     Coding clas,
                     Period period,
                     String duration,
                     Set<CodeableConcept> type,
                     Set<Encounter_StatusHistory> statusHistory,
                     Set<Reference> episodeOfCare,
                     Set<Reference> appointment,
                     Set<Reference> reasonReference,
                     Set<Encounter_Diagnosis> diagnosis,
                     Reference partof)
    { 
        super();
        this.identifier = identifier;
        this.status = status;
        this.clas = clas;
        this.period = period;
        this.duration = duration;
	    this.type=type;
    	this.statusHistory=statusHistory;
        this.episodeOfCare=episodeOfCare;
        this.appointment=appointment;
        this.reasonReference=reasonReference;
        this.diagnosis =diagnosis ;
        this.partof=partof;
    }

    public Encounter(Set<Identifier> identifier, StatusCode status, Coding clas, Period period, String duration) {
        super();
        this.identifier = identifier;
        this.status = status;
        this.clas = clas;
        this.period = period;
        this.duration = duration;
    }

    @OneToMany(cascade = CascadeType.ALL,targetEntity= Identifier.class,fetch= FetchType.EAGER)
    @JoinColumn(name="en_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();

    public enum StatusCode{
        planned("planned"),
        arrived("arrived"),
        triaged("triaged"),
        inprogress("in-progress"),
        onleave("onleave"),
        finished("finished"),
        cancelled("cancelled");
        private String value;
        private StatusCode(String value)
        {
            this.value = value;
        }

        public String toString()
        {
            return this.value;
        }
    }
    @Enumerated(EnumType.STRING)
    @Column(name="en_status")
    private StatusCode status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="en_class")
    @JsonProperty("class")
    private Coding clas;

    @Column(name = "en_period")
    private Period period;

    @Column(name="en_duration")
    private String duration;

    @OneToMany(cascade=CascadeType.ALL,targetEntity=CodeableConcept.class,fetch=FetchType.EAGER)
    @JoinColumn(name="en_type",referencedColumnName="id")
    private Set<CodeableConcept> type=new HashSet<>();

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Encounter_StatusHistory.class,fetch=FetchType.EAGER)
    @JoinColumn(name="en_status",referencedColumnName="id")
    private Set<Encounter_StatusHistory> statusHistory=new HashSet<>();
 
    @OneToOne
    private Reference subject;

    @OneToMany(cascade=CascadeType.ALL,targetEntity=Reference.class,fetch=FetchType.EAGER)
    @JoinColumn(name="en_episodeOfCare",referencedColumnName="id")
    private Set<Reference> episodeOfCare=new HashSet<>();

    @OneToMany(cascade=CascadeType.ALL,targetEntity=Reference.class,fetch=FetchType.EAGER)
    @JoinColumn(name="en_appointment",referencedColumnName="id")
    private Set<Reference> appointment=new HashSet<>();//Appointment

    @OneToMany(cascade=CascadeType.ALL,targetEntity=Reference.class,fetch=FetchType.EAGER)
    @JoinColumn(name="en_reasonReference",referencedColumnName="id")
    private Set<Reference> reasonReference=new HashSet<>();

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Encounter_Diagnosis.class,fetch=FetchType.EAGER)
    @JoinColumn(name="en_diagnosis",referencedColumnName="id")
    private Set<Encounter_Diagnosis> diagnosis =new HashSet<>();

    @OneToOne
    private Reference partof;//Encounter
}
