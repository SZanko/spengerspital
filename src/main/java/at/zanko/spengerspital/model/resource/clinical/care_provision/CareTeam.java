package at.zanko.spengerspital.model.resource.clinical.care_provision;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.CascadeType;

import at.zanko.spengerspital.model.type.resource.clinical.care_provision.CareTeam_Participant;
import at.zanko.spengerspital.model.type.other.DomainResource;
import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.data.Period;
import at.zanko.spengerspital.model.type.data.Reference;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/careteam.html> CareTeam Resource </a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10
 */

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public @Data
class CareTeam extends DomainResource {
    
	@OneToMany(cascade=CascadeType.ALL,targetEntity= Identifier.class,fetch=FetchType.EAGER)
	@JoinColumn(name="caretm_identifier",referencedColumnName="id")
	public Set<Identifier> identifier=new HashSet<>();

	public enum StatusCode{
		active("active"),
		inactive("inactive"),
		entered_in_error("entered-in-error"),
		onhold("onhold"),
		unknown("unknown");

		private String value;
		private StatusCode(String value){
			this.value=value;
		}
		public String toString(){

			return this.value;
		}
	}

	@Enumerated(EnumType.STRING)
	private StatusCode status;

	@OneToMany(cascade=CascadeType.ALL,targetEntity= CodeableConcept.class,fetch=FetchType.EAGER)
	@JoinColumn(name="caretm_category",referencedColumnName="id")
	private Set<CodeableConcept> category=new HashSet<>();

	@Column(name="caretm_name")
	private String name;

	@OneToOne
	@JoinColumn(name="caretm_subject")
	private Reference subject;//Patient,Group

	@OneToOne
	@JoinColumn(name="caretm_encounter")
	private Reference encounter;

	@Column(name="caretm_period")
	private Period period;

	@Embedded
	private Set<CareTeam_Participant> participant=new HashSet<>();

	@OneToMany(cascade=CascadeType.ALL,targetEntity= CodeableConcept.class,fetch=FetchType.EAGER)
	@JoinColumn(name="caretm_reason",referencedColumnName="id")
	private Set<CodeableConcept> reasonCode=new HashSet<>();

}
