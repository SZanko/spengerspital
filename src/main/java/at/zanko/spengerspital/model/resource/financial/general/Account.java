package at.zanko.spengerspital.model.resource.financial.general;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.resource.finical.general.Account_Coverage;
import at.zanko.spengerspital.model.type.other.DomainResource;
import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.data.Period;
import at.zanko.spengerspital.model.type.data.Reference;
import at.zanko.spengerspital.model.type.resource.finical.general.Account_Guarantor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <a href = https://www.hl7.org/fhir/account.html> Account Resource</a>
 *
 * @author	Stefan Zanko
 * @version	1.0
 * @since	2020-04-10
 */

@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Account extends DomainResource {
    @OneToMany(cascade=CascadeType.ALL,targetEntity= Identifier.class,fetch=FetchType.EAGER)
    @JoinColumn(name="accnt_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();

    public enum AccountStatusCode{
        active("active"),
        inactive("inactive"),
        entered_in_error("entered-in-error"),
        onhold("onhold"),
        unknown("unknown");

        private final String value;
        AccountStatusCode(String value) {
            this.value=value;
        }
        public String toString(){
            return this.value;
        }
    }
    @Enumerated(EnumType.STRING)
    private AccountStatusCode status;

    @OneToOne
    @JoinColumn(name="accnt_type")
    private CodeableConcept type;

    @Column(name="accnt_name")
    private String name;

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Reference.class,fetch=FetchType.EAGER)
    @JoinColumn(name="accnt_subject",referencedColumnName="id")
    private Set<Reference> subject=new HashSet<>();

    @Embedded
    private Period servicePeriod;

    @Embedded
    //@OneToMany(cascade=CascadeType.ALL,targetEntity=AccountCoverage.class,fetch=FetchType.EAGER)
    //@JoinColumn(name="acct_coverage",referencedColumnName="id")
    private Set<Account_Coverage> coverage=new HashSet<>();

    @OneToOne
    @JoinColumn(name="accnt_owner")
    private Reference owner;

    @Column(name="accnt_description")
    private String description;

    @Embedded
    //@OneToMany(cascade=CascadeType.ALL,targetEntity=guarantor.class,fetch=FetchType.EAGER)
    //@JoinColumn(name="acct_guarantor",referencedColumnName="id")
    private Set<Account_Guarantor> Account_Guarantor =new HashSet<>();

	@OneToOne
    @JoinColumn(name="accnt_partOf")
    private Reference partOf;
}
