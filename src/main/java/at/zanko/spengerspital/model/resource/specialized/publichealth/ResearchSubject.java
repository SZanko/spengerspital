package at.zanko.spengerspital.model.resource.specialized.publichealth;

import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.data.Period;
import at.zanko.spengerspital.model.type.data.Reference;
import at.zanko.spengerspital.model.type.other.DomainResource;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public @Data
class ResearchSubject extends DomainResource {

    @OneToOne
    private Identifier identifier;

    public enum StatiCode{
        candit("candit"),
        enroll("en-roll"),
        inactive("in-active"),
        suspend("suspend"),
        withdrawn("withdrawn"),
        completed("completed");

        private String value;

        StatiCode(String value) {
            this.value=value;
        }

        @Override
        public String toString() {
            return this.value;
        }

    }

    @Enumerated(EnumType.STRING)
    @NonNull
    private StatiCode status;

    @Embedded
    private Period period;


    @OneToOne(cascade = CascadeType.ALL)
    @NonNull
    private Reference indiviualization;

    @Column
    private LocalDateTime datetime;
}
