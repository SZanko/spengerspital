package at.zanko.spengerspital.model.resource.clinical.medications;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

import at.zanko.spengerspital.model.type.other.DomainResource;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.model.type.resource.clinical.medications.Medication_Batch;
import at.zanko.spengerspital.model.type.resource.clinical.medications.Medication_Ingredient;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/medication.html> Medication Resource </a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since 2020-09-16	
 */

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Entity
@NoArgsConstructor
public @Data
class Medication extends DomainResource{
	@OneToMany(cascade=CascadeType.PERSIST,targetEntity= Identifier.class,fetch=FetchType.EAGER)
	@JoinColumn(name="medication_identifier",referencedColumnName="id")
	public Set<Identifier> identifier=new HashSet<>();

	@OneToOne
	@JoinColumn(name = "medication_code")
	public CodeableConcept code;

	public enum StatusCode{
		active("active"),
		inactive("inactive"),
		entered_in_error("entered-in-error");
		private final String value;
		private StatusCode(String value)
		{
			this.value=value;
		}

		public String toString()
		{
			return this.value;
		}
	}

	@Enumerated(EnumType.STRING)
	@Column(name="medication_status")
	private StatusCode status;

	@OneToOne
	@JoinColumn(name="medication_manufacturer")
	private Reference Reference;

	@OneToOne
	@JoinColumn(name="medication_form")
	private CodeableConcept form;

	@OneToOne
	@JoinColumn(name="medication_amount")
	private Ratio amount;

	@OneToMany(cascade= CascadeType.PERSIST,targetEntity = Medication_Ingredient.class,fetch = FetchType.EAGER)
	@JoinColumn(name="med_identifier",referencedColumnName = "id")
	private Set<Medication_Ingredient> medication_ingredients;

	@OneToOne
	@JoinColumn(name="medication_batch")
	private Medication_Batch batch;
}
