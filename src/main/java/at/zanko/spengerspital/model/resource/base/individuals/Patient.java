package at.zanko.spengerspital.model.resource.base.individuals;

import javax.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import at.zanko.spengerspital.model.type.other.DomainResource;
import at.zanko.spengerspital.model.type.data.ContactPoint;
import at.zanko.spengerspital.model.type.data.HumanName;
import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.data.Address;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/patient.html> Patient Resource</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class Patient extends DomainResource {

    public Patient(
            boolean active,
            GenderCode gender,
            LocalDate birthDate,
            Set<Identifier> identifier,
            Set<HumanName> humanname,
            Set<ContactPoint> phonenumbers
    ) {
        super();
        this.active = active;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    public enum GenderCode{
        male,female,other,unknown
    }

    @Column(name="p_active") 
    private boolean active;
    
    @Enumerated(EnumType.STRING)
    @Column(name="p_gender")
    private GenderCode gender=GenderCode.unknown;

    @Column(name="p_birthdate")
    private LocalDate birthDate;

    @OneToMany(cascade= CascadeType.ALL,targetEntity = Identifier.class,fetch = FetchType.EAGER)
    @JoinColumn(name="p_identifier",referencedColumnName = "id")
    private Set<Identifier> identifier;

    @OneToMany(cascade= CascadeType.ALL,targetEntity = HumanName.class,fetch = FetchType.EAGER)
    @JoinColumn(name="patient_name",referencedColumnName = "id")
    private Set<HumanName> name;

    @OneToMany(cascade= CascadeType.ALL,targetEntity = ContactPoint.class,fetch = FetchType.EAGER)
    @JoinColumn(name="p_telecom",referencedColumnName = "id")
    private Set<ContactPoint> telecom;

    @Column(name = "p_deceased")
    private LocalDateTime deceased;

    @Column(name="p_address")
    @OneToMany(cascade= CascadeType.ALL,targetEntity = Address.class,fetch = FetchType.EAGER)
    @JoinColumn(name="p_address",referencedColumnName = "id")
    private Set<Address> address;
}
