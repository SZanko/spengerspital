package at.zanko.spengerspital.model.resource.base.management;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import at.zanko.spengerspital.model.type.data.Reference;
import at.zanko.spengerspital.model.type.other.DomainResource;

import at.zanko.spengerspital.model.type.data.CodeableConcept;
import at.zanko.spengerspital.model.type.data.Identifier;
import at.zanko.spengerspital.model.type.data.Period;
import lombok.*;

/**
 * <a href = https://www.hl7.org/fhir/episodeofcare.html> EpisodeOfCare Resource</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-04-10
 */

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public @Data
class EpisodeOfCare extends DomainResource{

    @OneToMany(cascade=CascadeType.ALL,targetEntity= Identifier.class,fetch=FetchType.EAGER)
    @JoinColumn(name="epiofca_identifier",referencedColumnName="id")
    private Set<Identifier> identifier=new HashSet<>();

    public enum EpisodeOfCareStatus{
        planned("planned"),
        waitlist("waitlist"),
        active("active"),
        onhold("onhold"),
        finished("finished"),
        cancelled("cancelled"),
        entered_in_error("entered-in-error");

        private String value;
        private EpisodeOfCareStatus(String value){
            this.value=value;
        }
        public String toString(){
            return this.value;
        }
    }

    @Enumerated(EnumType.STRING)
    private EpisodeOfCareStatus statuscode;

    //TODO statushistory
    
    @OneToMany(cascade=CascadeType.ALL,targetEntity=CodeableConcept.class,fetch=FetchType.EAGER)
    @JoinColumn(name="epiofca_type",referencedColumnName="id")
    private Set<CodeableConcept> type=new HashSet<>();

    //TODO diagnosis
    
    @OneToOne
    private Reference patient;

    @OneToOne
    @JoinColumn(name="epiofca_managingOrganization")
    private Reference managingOrganization;

    @Column(name="epiofca_period")
    private Period period;

    //TODO ServiceRequest
    //@OneToMany(cascade=CascadeType.ALL,targetEntity=Reference.class,fetch=FetchType.EAGER)
    //@JoinColumn(name="epiofca_referralRequest",referencedColumnName="id")
    //private Reference referralRequest;

    //TODO PractitionerRole
    //@Column(name="epiofca_careManager")
    //private Reference careManager;

    ////TODO CareTeam
    //@Column(name="epiofca_team")
    //private Reference team;

    //@Column(name="epiofca_account")
    //private Reference account;

}
