package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.base.individuals.PractitionerRole;
import at.zanko.spengerspital.repositories.PractitionerRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@CrossOrigin
@RestController
@RequestMapping(path="/api/practitionerrole")
public class PractitionerRoleController {
    /**
     * The Repository which is used by this Controller
     */
    @Autowired
    private PractitionerRoleRepository practitionerroleRepository;

    /**
     * Return all PractitionerRoles in the Repository
     *
     * @return List PractitionerRole as Json
     */
    @GetMapping
    @Async
    public @ResponseBody
    Iterable<PractitionerRole>
    getAllPractitionerRole(){
        return practitionerroleRepository.findAll();
    }

    /**
     * Gets one PractitionerRole by their Id
     * at the url /api/practitionerrole/{id}
     *
     * @param id of the wanted PractitionerRole
     * @return PractitionerRole with the given id as a String
     */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<PractitionerRole>
    getPractitionerRole(@PathVariable String id){
        PractitionerRole practitionerrole= practitionerroleRepository.findById(id).get();
        if(practitionerrole== null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(practitionerrole);
    }

    /**
     * Saves one PractitionerRole to the Database
     *
     * @param practitionerrole the full PractitionerRole as a Json
     * @return Saved PractitionerRole
     */
    @PostMapping
    @Async
    public ResponseEntity<PractitionerRole>
    newPractitionerRoleRole(@RequestBody PractitionerRole practitionerrole){
        practitionerrole.setId(null); // ensure to create new names
        PractitionerRole saved = practitionerroleRepository.save(practitionerrole);
        return ResponseEntity.created(URI.create("/api/practitionerrole/" + saved.getId())).body(saved);
    }

    /**
     * Updates a PractitionerRole by their Id
     * at the url /api/practitionerrole/{id}
     *
     * @param newpractitionerrole full PractitionerRole as Json
     * @param id the Id as a String of the PractitionerRole which will be updated
     * @return the updated patient
     */
    @PutMapping("/{id}")
    @Async
    public PractitionerRole
    replacePractitionerRole(@RequestBody PractitionerRole newpractitionerrole, @PathVariable String id){
        return practitionerroleRepository.findById(id).map(
                practitionerrole ->{
                    practitionerrole.setActive(newpractitionerrole.isActive());
                    practitionerrole.setPractitioner(newpractitionerrole.getPractitioner());
                    practitionerrole.setCode(newpractitionerrole.getCode());
                    practitionerrole.setIdentifier(newpractitionerrole.getIdentifier());
                    practitionerrole.setAvailableTime(newpractitionerrole.getAvailableTime());
                    practitionerrole.setTelecom(newpractitionerrole.getTelecom());
                    practitionerrole.setLocation(newpractitionerrole.getLocation());
                    practitionerrole.setOrganization(newpractitionerrole.getOrganization());
                    practitionerrole.setPeriod(newpractitionerrole.getPeriod());
                    practitionerrole.setSpecialty(newpractitionerrole.getSpecialty());
                    practitionerrole.setHealthcareService(newpractitionerrole.getHealthcareService());
                    return practitionerroleRepository.save(newpractitionerrole);
                }
        ).orElseGet(()->{
            newpractitionerrole.setId(id);

            return practitionerroleRepository.save(newpractitionerrole);
        });
    }

    /**
     * Deletes a Practitioner by their Id
     * at the url /api/practitionerrole/{id}
     *
     * @param id the Id as a String of the Practitioner which will be deleted
     */
    @DeleteMapping("/{id}")
    @Async
    public void
    deletePractitioner(@PathVariable String id){
        practitionerroleRepository.deleteById(id);
    }
}
