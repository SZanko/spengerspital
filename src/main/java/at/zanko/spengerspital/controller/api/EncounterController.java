package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.base.management.Encounter;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;

import at.zanko.spengerspital.repositories.EncounterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * The Api Controller for the <a href = https://www.hl7.org/fhir/encounter.html>Encounter Resource</a> of the FHIR API
 * Implements <a href = https://de.wikipedia.org/wiki/CRUD>CRUD</a>
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-03-13
 */
@RestController
@RequestMapping(path="/api/encounter")
public class EncounterController{

	/**
	 * The Repository which is used by this Controller
	 */
	@Autowired
	private EncounterRepository encounterRepository;

	/**
	 * Return all Encounters in the Repository
	 *
	 * @return List from Encounter as Json
	 */
	@GetMapping
	@Async
	public @ResponseBody Iterable<Encounter>
	getAllEncounters(){
		return encounterRepository.findAll();
	}

	/**
	 * Gets one Encounter by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param id of the wanted Encounter
	 * @return Encounter with the given id as a String
	 */
	@GetMapping("/{id}")
	@Async
	public ResponseEntity<Encounter>
	getAEncounter(@PathVariable String id){
		Encounter encounter = encounterRepository.findById(id).get();

		if(encounter == null) {

			return ResponseEntity.notFound().build();

		}

		return ResponseEntity.ok().body(encounter); 
	}

	/**
	 * Saves one Encounter to the Database
	 *
	 * @param encounter the full Encounter as a Json
	 * @return Saved Encounter
	 */
	@PostMapping
	@Async
	public Encounter
	newEncounter(@RequestBody Encounter encounter){
		return encounterRepository.save(encounter);
	}

	/**
	 * Updates a Encounter by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param newencounter the full Encounter as Json
	 * @param id the Id as a String of the Encounter which will be updated
	 * @return the updated Encounter
	 */
	@PutMapping("/{id}")
	@Async
	public Encounter
	replaceencounter(@RequestBody Encounter newencounter, @PathVariable String id){
		return encounterRepository.findById(id).map(
				encounter ->{
					encounter.setIdentifier(newencounter.getIdentifier());
					encounter.setStatus(newencounter.getStatus());
					encounter.setClas(newencounter.getClas());
					encounter.setPeriod(newencounter.getPeriod());
					encounter.setDuration(newencounter.getDuration());
					encounter.setType(newencounter.getType());
					encounter.setStatus(newencounter.getStatus());
					encounter.setEpisodeOfCare(newencounter.getEpisodeOfCare());
					encounter.setAppointment(newencounter.getAppointment());
					encounter.setReasonReference(newencounter.getReasonReference());
					encounter.setDiagnosis(newencounter.getDiagnosis());
					encounter.setPartof(newencounter.getPartof());
					return encounterRepository.save(newencounter);
				}
				).orElseGet(()->{
			newencounter.setId(id);

			return encounterRepository.save(newencounter);
		});
	}

	/**
	 * Deletes a Encounter by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param id the Id as a String of the Encounter which will be deleted
	 */
	@DeleteMapping("/{id}")
	@Async
	public void
	deleteAEncounter(@PathVariable String id){
		encounterRepository.deleteById(id);
	}
}
