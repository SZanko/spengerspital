package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.specialized.publichealth.ResearchSubject;
import at.zanko.spengerspital.repositories.ResearchSubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@CrossOrigin
@RestController
@RequestMapping(path="/api/researchsubject")
public class ResearchSubjectController {
    /**
     * The Repository which is used by this Controller
     */
    @Autowired
    private ResearchSubjectRepository researchSubjectRepository;

    /**
     * Return all ResearchSubjects in the Repository
     *
     * @return List ResearchSubject as Json
     */
    @GetMapping
    @Async
    public @ResponseBody
    Iterable<ResearchSubject>
    getAllResearchSubject(){
        return researchSubjectRepository.findAll();
    }

    /**
     * Gets one ResearchSubject by their Id
     * at the url /api/researchSubject/{id}
     *
     * @param id of the wanted ResearchSubject
     * @return ResearchSubject with the given id as a String
     */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<ResearchSubject>
    getResearchSubject(@PathVariable String id){
        ResearchSubject researchSubject= researchSubjectRepository.findById(id).get();
        if(researchSubject== null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(researchSubject);
    }

    /**
     * Saves one ResearchSubject to the Database
     *
     * @param researchSubject the full ResearchSubject as a Json
     * @return Saved ResearchSubject
     */
    @PostMapping
    @Async
    public ResponseEntity<ResearchSubject>
    newResearchSubject(@RequestBody ResearchSubject researchSubject){
        researchSubject.setId(null); // ensure to create new names
        ResearchSubject saved = researchSubjectRepository.save(researchSubject);
        return ResponseEntity.created(URI.create("/api/researchSubject/" + saved.getId())).body(saved);
    }

    /**
     * Updates a ResearchSubject by their Id
     * at the url /api/researchSubject/{id}
     *
     * @param newresearchSubject full ResearchSubject as Json
     * @param id the Id as a String of the ResearchSubject which will be updated
     * @return the updated patient
     */
    @PutMapping("/{id}")
    @Async
    public ResearchSubject
    replaceResearchSubject(@RequestBody ResearchSubject newresearchSubject, @PathVariable String id){
        return researchSubjectRepository.findById(id).map(
                researchSubject ->{
                    researchSubject.setIdentifier(newresearchSubject.getIdentifier());
                    researchSubject.setDatetime(newresearchSubject.getDatetime());
                    researchSubject.setIndiviualization(newresearchSubject.getIndiviualization());
                    researchSubject.setPeriod(newresearchSubject.getPeriod());
                    researchSubject.setStatus(newresearchSubject.getStatus());
                    return researchSubjectRepository.save(newresearchSubject);
                }
        ).orElseGet(()->{
            newresearchSubject.setId(id);

            return researchSubjectRepository.save(newresearchSubject);
        });
    }

    /**
     * Deletes a ResearchSubject by their Id
     * at the url /api/researchSubject/{id}
     *
     * @param id the Id as a String of the ResearchSubject which will be deleted
     */
    @DeleteMapping("/{id}")
    @Async
    public void
    deleteResearchSubject(@PathVariable String id) {
        researchSubjectRepository.deleteById(id);
    }
}
