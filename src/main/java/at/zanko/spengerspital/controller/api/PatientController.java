package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.base.individuals.Patient;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;

import at.zanko.spengerspital.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

/**
 * The Api Controller for the <a href = https://www.hl7.org/fhir/patient.html>Patient Resource</a> of the FHIR API
 * Implements <a href = https://de.wikipedia.org/wiki/CRUD>CRUD</a>
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */

@CrossOrigin
@RestController
@RequestMapping(path="/api/patient")
public class PatientController {

	/**
	 * The Repository which is used by this Controller
	 */
    @Autowired
    private PatientRepository patientRepository;

	/**
	 * Return all Patients in the Repository
	 *
	 * @return List Patient as Json
	 */
    @GetMapping
    @Async
    public @ResponseBody Iterable<Patient>
    getAllPatients(){
        return patientRepository.findAll();
    }

	/**
	 * Gets one Patient by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param id of the wanted Patient
	 * @return Patient with the given id as a String
	 */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<Patient>
    getPatient(@PathVariable String id){
        Patient patient = patientRepository.findById(id).get();

        if(patient == null) {

            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok().body(patient);
    }

	/**
	 * Saves one Patient to the Database
	 *
	 * @param patient the full Patient as a Json
	 * @return Saved Patient
	 */
    @PostMapping()
    @Async
    public ResponseEntity<Patient>
    newPatient(@RequestBody Patient patient){
        patient.setId(null); // ensure to create new names
        Patient saved = patientRepository.save(patient);
        return ResponseEntity.created(URI.create("/api/patient/" + saved.getId())).body(saved);
    }
    
    /**
	 * Updates a Patient by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param newpatient the full Patient as Json
	 * @param id the Id as a String of the Patient which will be updated
     * @return the updated Patient
	 */
    @PutMapping("/{id}")
    @Async
    public Patient
    replacepatient(@RequestBody Patient newpatient, @PathVariable String id){
        return patientRepository.findById(id).map(
                patient ->{
                    patient.setActive(newpatient.isActive());
                    patient.setAddress(newpatient.getAddress());
                    patient.setBirthDate(newpatient.getBirthDate());
                    patient.setDeceased(newpatient.getDeceased());
                    patient.setGender(newpatient.getGender());
                    patient.setIdentifier(newpatient.getIdentifier());
                    patient.setName(newpatient.getName());
                    patient.setTelecom(newpatient.getTelecom());
                    return patientRepository.save(newpatient);
                }
                ).orElseGet(()->{
            newpatient.setId(id);
        
            return patientRepository.save(newpatient);
        });
    }

    /**
	 * Deletes a Patient by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param id the Id as a String of the Patient which will be deleted
	 */
    @DeleteMapping("/{id}")
    @Async
    public void
    deletePatient(@PathVariable String id){
        patientRepository.deleteById(id);
    }

}
