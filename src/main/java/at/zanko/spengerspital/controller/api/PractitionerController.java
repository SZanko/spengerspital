package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.base.individuals.Practitioner;
import at.zanko.spengerspital.repositories.PractitionerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

/**
 * The Api Controller for the <a href = https://www.hl7.org/fhir/practitioner.html>Practitioner Resource</a> of the FHIR API
 * Implements <a href = https://de.wikipedia.org/wiki/CRUD>CRUD</a>
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-03-13
 */

@CrossOrigin
@RestController
@RequestMapping(path="/api/practitioner")
public class PractitionerController{

	/**
	 * The Repository which is used by this Controller
	 */
    @Autowired
    private PractitionerRepository practitionerRepository;

	/**
	 * Return all Practitioners in the Repository
	 *
	 * @return List Practitioner as Json
	 */
    @GetMapping
    @Async
    public @ResponseBody Iterable<Practitioner>
    getAllPractitioner(){
        return practitionerRepository.findAll();
    }

	/**
	 * Gets one Practitioner by their Id
	 * at the url /api/practitioner/{id}
	 *
	 * @param id of the wanted Practitioner
	 * @return Practitioner with the given id as a String
	 */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<Practitioner>
    getPractitioner(@PathVariable String id){
        Practitioner practitioner= practitionerRepository.findById(id).get();
        if(practitioner== null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(practitioner);
    }

	/**
	 * Saves one Practitioner to the Database
	 *
	 * @param practitioner the full Practitioner as a Json
	 * @return Saved Practitioner
	 */
    @PostMapping
    @Async
    public ResponseEntity<Practitioner>
    newPractitioner(@RequestBody Practitioner practitioner){
        practitioner.setId(null); // ensure to create new names
        Practitioner saved = practitionerRepository.save(practitioner);
        return ResponseEntity.created(URI.create("/api/practitioner/" + saved.getId())).body(saved);
    }
    
    /**
	 * Updates a Practitioner by their Id
	 * at the url /api/practitioner/{id}
	 *
	 * @param newpractitioner full Practitioner as Json
	 * @param id the Id as a String of the Practitioner which will be updated
     * @return the updated patient
	 */
    @PutMapping("/{id}")
    @Async
    public Practitioner
    replacePractitioner(@RequestBody Practitioner newpractitioner, @PathVariable String id){
        return practitionerRepository.findById(id).map(
                practitioner ->{
                    practitioner.setName(newpractitioner.getName());
                    practitioner.setTelecom(newpractitioner.getTelecom());
                    practitioner.setPhoto(newpractitioner.getPhoto());
                    practitioner.setActive(newpractitioner.isActive());
                    practitioner.setGender(newpractitioner.getGender());
                    practitioner.setAddress(newpractitioner.getAddress());
                    practitioner.setIdentifier(newpractitioner.getIdentifier());
                    practitioner.setCommunication(newpractitioner.getCommunication());
                    practitioner.setPractitionerQualification(newpractitioner.getPractitionerQualification());
                    practitioner.setBirthDate(newpractitioner.getBirthDate());
                    return practitionerRepository.save(newpractitioner);
                }
                ).orElseGet(()->{
            newpractitioner.setId(id);
        
            return practitionerRepository.save(newpractitioner);
        });
    }

    /**
	 * Deletes a Practitioner by their Id
	 * at the url /api/practitioner/{id}
	 *
	 * @param id the Id as a String of the Practitioner which will be deleted
	 */
    @DeleteMapping("/{id}")
    @Async
    public void
    deletePractitioner(@PathVariable String id){
        practitionerRepository.deleteById(id);
    }
}
