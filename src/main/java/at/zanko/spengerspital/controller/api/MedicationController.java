package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.clinical.medications.Medication;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;

import at.zanko.spengerspital.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * The Api Controller for the <a href = https://www.hl7.org/fhir/medication.html>Medication Resource</a> of the FHIR API
 * Implements <a href = https://de.wikipedia.org/wiki/CRUD>CRUD</a>
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-03-23
 */

@CrossOrigin
@RestController
@RequestMapping(path="/api/medication")
public class MedicationController {

	/**
	 * The Repository which is used by this Controller
	 */
    @Autowired
    private MedicationRepository medicationRepository;

	/**
	 * Return all Medications in the Repository
	 *
	 * @return List Medication as Json
	 */
    @GetMapping
    @Async
    public @ResponseBody Iterable<Medication>
    getAllMedications(){
        return medicationRepository.findAll();
    }

	/**
	 * Gets one Medication by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param id of the wanted Medication
	 * @return Medication with the given id as a String
	 */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<Medication>
    getMedication(@PathVariable String id){
        Medication medication = medicationRepository.findById(id).get();

        if(medication == null) {

            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok().body(medication);
    }

	/**
	 * Saves one Medication to the Database
	 *
	 * @param medication the full Medication as a Json
	 * @return Saved Medication
	 */
    @PostMapping
    @Async
    public Medication
    newMedication(@RequestBody Medication medication){
        return medicationRepository.save(medication);
    }
    
    /**
	 * Updates a Medication by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param newmedication the full Medication as Json
	 * @param id the Id as a String of the Medication which will be updated
     * @return the updated Medication
	 */
    @PutMapping("/{id}")
    @Async
    public Medication
    replacemedication(@RequestBody Medication newmedication, @PathVariable String id){
        return medicationRepository.findById(id).map(
                medication ->{
                    medication.setAmount(newmedication.getAmount());
                    medication.setForm(newmedication.getForm());
                    medication.setId(newmedication.getId());
                    medication.setReference(newmedication.getReference());
                    medication.setStatus(newmedication.getStatus());
                    medication.setIdentifier(newmedication.getIdentifier());
                    return medicationRepository.save(newmedication);
                }
                ).orElseGet(()->{
            newmedication.setId(id);
        
            return medicationRepository.save(newmedication);
        });
    }

    /**
	 * Deletes a Medication by their Id
	 * at the url /api/encounter/{id}
	 *
	 * @param id the Id as a String of the Medication which will be deleted
	 */
    @DeleteMapping("/{id}")
    @Async
    public void
    deleteMedication(@PathVariable String id){
        medicationRepository.deleteById(id);
    }

}
