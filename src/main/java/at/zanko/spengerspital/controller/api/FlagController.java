package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.resource.base.individuals.Practitioner;
import at.zanko.spengerspital.model.type.data.Flag;
import at.zanko.spengerspital.repositories.FlagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@CrossOrigin
@RestController
@RequestMapping(path="/api/flag")
public class FlagController {

    /**
     * The Repository which is used by this Controller
     */
    @Autowired
    private FlagRepository flagRepository;

    /**
     * Return all Flags in the Repository
     *
     * @return List Flag as Json
     */
    @GetMapping
    @Async
    public @ResponseBody
    Iterable<Flag>
    getAllFlags(){
        return flagRepository.findAll();
    }

    /**
     * Gets one Flag by their Id
     * at the url /api/encounter/{id}
     *
     * @param id of the wanted Flag
     * @return Flag with the given id as a String
     */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<Flag>
    getFlag(@PathVariable String id){
        Flag flag = flagRepository.findById(id).get();

        if(flag == null) {

            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok().body(flag);
    }

    /**
     * Saves one Flag to the Database
     *
     * @param flag the full Flag as a Json
     * @return Saved Flag
     */
    @PostMapping
    @Async
    public ResponseEntity<Flag>
    newFlag(@RequestBody Flag flag){
        flag.setId(null); // ensure to create new names
        Flag saved = flagRepository.save(flag);
        return ResponseEntity.created(URI.create("/api/practitioner/" + saved.getId())).body(saved);
    }

    /**
     * Updates a Flag by their Id
     * at the url /api/encounter/{id}
     *
     * @param newflag the full Flag as Json
     * @param id the Id as a String of the Flag which will be updated
     * @return the updated Flag
     */
    @PutMapping("/{id}")
    @Async
    public Flag
    replaceflag(@RequestBody Flag newflag, @PathVariable String id){
        return flagRepository.findById(id).map(
                flag ->{
                                flag.setAuthor(newflag.getAuthor());
                    flag.setId(newflag.getId());
                    flag.setAuthor(newflag.getAuthor());
                            flag.setStatus(newflag.getStatus());
                    flag.setIdentifier(newflag.getIdentifier());
                    return flagRepository.save(newflag);
                }
        ).orElseGet(()->{
            newflag.setId(id);

            return flagRepository.save(newflag);
        });
    }

    /**
     * Deletes a Flag by their Id
     * at the url /api/encounter/{id}
     *
     * @param id the Id as a String of the Flag which will be deleted
     */
    @DeleteMapping("/{id}")
    @Async
    public void
    deleteFlag(@PathVariable String id){
        flagRepository.deleteById(id);
    }

}
