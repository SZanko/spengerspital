package at.zanko.spengerspital.controller.api;

import at.zanko.spengerspital.model.type.data.DeviceUseStatement;
import at.zanko.spengerspital.repositories.DeviceUseStatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

/**
 * <a href = https://www.hl7.org/fhir/deviceusestatement-definitions.html#DeviceUseStatement> Device Use Statment</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-11-11
 */
@RestController
@RequestMapping(path="/api/deviceUseStatement")
public class DeviceUseStatementController {

    /**
     * The Repository which is used by this Controller
     */
    @Autowired
    private DeviceUseStatementRepository deviceUseStatementRepository;

    /**
     * Return all DeviceUseStatements in the Repository
     *
     * @return List DeviceUseStatement as Json
     */
    @GetMapping
    @Async
    public @ResponseBody
    Iterable<DeviceUseStatement>
    getAllDeviceUseStatements(){
        return deviceUseStatementRepository.findAll();
    }

    /**
     * Gets one DeviceUseStatement by their Id
     * at the url /api/encounter/{id}
     *
     * @param id of the wanted DeviceUseStatement
     * @return DeviceUseStatement with the given id as a String
     */
    @GetMapping("/{id}")
    @Async
    public ResponseEntity<DeviceUseStatement>
    getDeviceUseStatement(@PathVariable String id){
        DeviceUseStatement deviceUseStatement = deviceUseStatementRepository.findById(id).get();

        if(deviceUseStatement == null) {

            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok().body(deviceUseStatement);
    }

    /**
     * Saves one DeviceUseStatement to the Database
     *
     * @param deviceUseStatement the full DeviceUseStatement as a Json
     * @return Saved DeviceUseStatement
     */
    @PostMapping
    @Async
    public DeviceUseStatement
    newDeviceUseStatement(@RequestBody DeviceUseStatement deviceUseStatement){
        return deviceUseStatementRepository.save(deviceUseStatement);
    }

    /**
     * Updates a DeviceUseStatement by their Id
     * at the url /api/encounter/{id}
     *
     * @param newdeviceUseStatement the full DeviceUseStatement as Json
     * @param id the Id as a String of the DeviceUseStatement which will be updated
     * @return the updated DeviceUseStatement
     */
    @PutMapping("/{id}")
    @Async
    public DeviceUseStatement
    replacedeviceUseStatement(@RequestBody DeviceUseStatement newdeviceUseStatement, @PathVariable String id){
        return deviceUseStatementRepository.findById(id).map(
                deviceUseStatement ->{
                    deviceUseStatement.setBasedOn(newdeviceUseStatement.getBasedOn());
                    deviceUseStatement.setIdentifier(newdeviceUseStatement.getIdentifier());
                    deviceUseStatement.setDeviceUseStatement_status(newdeviceUseStatement.getDeviceUseStatement_status());
                    deviceUseStatement.setId(newdeviceUseStatement.getId());
                    deviceUseStatement.setRecordedOn(newdeviceUseStatement.getRecordedOn());
                    deviceUseStatement.setTiming(newdeviceUseStatement.getTiming());
                    return deviceUseStatementRepository.save(newdeviceUseStatement);
                }
        ).orElseGet(()->{
            newdeviceUseStatement.setId(id);

            return deviceUseStatementRepository.save(newdeviceUseStatement);
        });
    }

    /**
     * Deletes a DeviceUseStatement by their Id
     * at the url /api/encounter/{id}
     *
     * @param id the Id as a String of the DeviceUseStatement which will be deleted
     */
    @DeleteMapping("/{id}")
    @Async
    public void
    deleteDeviceUseStatement(@PathVariable String id){
        deviceUseStatementRepository.deleteById(id);
    }

}
