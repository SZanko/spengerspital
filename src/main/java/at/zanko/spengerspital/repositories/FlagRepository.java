package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.type.data.Flag;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlagRepository extends PagingAndSortingRepository<Flag, String> {

}
