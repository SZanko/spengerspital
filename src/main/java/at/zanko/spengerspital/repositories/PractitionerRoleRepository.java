package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.resource.base.individuals.PractitionerRole;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PractitionerRoleRepository extends PagingAndSortingRepository<PractitionerRole,String> {
}
