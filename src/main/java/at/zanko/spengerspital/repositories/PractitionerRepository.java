package at.zanko.spengerspital.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.PagingAndSortingRepository;
import at.zanko.spengerspital.model.resource.base.individuals.Practitioner;

/**
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */
@Repository
public interface PractitionerRepository extends PagingAndSortingRepository<Practitioner,String>{

}
