package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.resource.base.individuals.Patient;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/**
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-02-21
 */
@Repository
public interface PatientRepository extends PagingAndSortingRepository<Patient, String> {

}
