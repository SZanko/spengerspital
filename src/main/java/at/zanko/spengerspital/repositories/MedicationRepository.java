package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.resource.clinical.medications.Medication;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/**
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-09-23
 */
@Repository
public interface MedicationRepository extends PagingAndSortingRepository<Medication, String> {

}
