package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.type.data.DeviceUseStatement;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * <a href = https://www.hl7.org/fhir/deviceusestatement-definitions.html#DeviceUseStatement> Device Use Statment</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-11-11
 */
@Repository
public interface DeviceUseStatementRepository extends PagingAndSortingRepository<DeviceUseStatement,String> {

}
