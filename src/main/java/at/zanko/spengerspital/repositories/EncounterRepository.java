package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.resource.base.management.Encounter;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/**
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-03-13
 */
@Repository
public interface EncounterRepository extends PagingAndSortingRepository<Encounter, String> {

}
