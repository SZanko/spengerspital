package at.zanko.spengerspital.repositories;

import at.zanko.spengerspital.model.resource.specialized.publichealth.ResearchSubject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResearchSubjectRepository extends PagingAndSortingRepository<ResearchSubject,String> {

}
