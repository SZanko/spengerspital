--  Wichtig wenn Hibernate die Tabelle erstellt wird kein Autoincrement übergeben

-- Quantity
Insert into quantity(id,comparator,system,unit,value) values('1','smaller','kein system','keine unit',5.6), ('3','smaller','kein system','keine unit',5.7), ('2','smaller','dreck system', 'drecks unit',5.6);

-- Codeable Concept
insert into codeable_concept(id,cc_text) values ('123123','Test'), ('1435o2','TestDreck');

-- Coding
INSERT INTO coding (id,c_code,c_display,c_system,c_user_selected,c_version) VALUES ('1','keine Ahnung Code','testcode','http://test.identity.at/',false,'0.1');

-- Patient
INSERT into patient(id,p_active,p_birthdate,p_gender,p_deceased) VALUES ('Test',1,'2001-11-9','male','2001-11-9'),('OpferPatient',0,'2001-11-19','female','2001-11-9');

-- Address
INSERT INTO address (id,add_city,add_country,pe_end,pe_start,add_postal_code,add_state,add_text,add_type,add_use,p_address) VALUES ('1','Vienna','Austria','2000-02-02','1999-01-01','1100','W','Satans Küche','both','temp','OpferPatient');

-- Practitioner
INSERT INTO practitioner (id,prac_inactive,prac_birthdate,prac_gender) VALUES ('23',1,'2000-02-02','male');

-- Contact Point
INSERT INTO contact_point(id,pe_end,pe_start,ctp_value,ctp_rank,ctp_system,ctp_use,ctp_prac_fk) values('Dreckspunkt','2000-02-02','1999-01-01','ctp-value',3,'sms','home','23')

-- Identifier
INSERT INTO identifier(id,code,i_system,i_value,i_codeableconcept_fk,pe_end,pe_start,p_identifier,prac_identifier) VALUES ('230', 'secondary', 'http://url', '123123321','123123','2006-06-06','2007-09-09','Test','23');

-- HumanName
insert into human_name(id,pe_start,pe_end,hun_text,hun_use,hun_family,patient_name,prac_name_fk) values ('123123','1999-01-01','1999-01-01','hun-text','old','hun-family','Test','23');

-- Encounter
INSERT INTO encounter (id,en_status,en_class) VALUES ('666','finished',null),('777','finished',null);

-- Reference
INSERT INTO rf_reference (id,rf_display,rf_reference,rf_type,rf_identifier) values('1','alternativer Text','TestReference','https://test.example.com','230');

-- Ratio
INSERT INTO ratio(id,denominator_id,numerity_id) VALUES ('1','1','1'),('3','2','3');

-- Medication
-- INSERT INTO medication(id,medication_status,medication_code,medication_amount,medication_form) VALUES ('666','active','1435o2','1','123123');

INSERT INTO device_use_statement(id,device_use_statement_status,devices_use_statement_recorded_on,timing_timing_date_time,pe_end,pe_start) Values ('666','active','1999-01-01','1999-01-01','1999-01-01','1999-01-01');

-- Flag
Insert Into flag(id,status,author_id,pe_start,pe_end) values ('Test','activity','1','1999-01-01','1999-01-01');

-- PractitionerRole
Insert into practitioner_role(id,active,pe_start,pe_end) values ('Hanswurst',true,'1999-01-01','1999-01-01');

-- ResearchSubject
Insert into research_subject(id,datetime,pe_start,pe_end,status,identifier_id, indiviualization_id) values ('tmpresearchsubject','1999-01-01','1999-01-01','1999-01-01','candit','230','1' )