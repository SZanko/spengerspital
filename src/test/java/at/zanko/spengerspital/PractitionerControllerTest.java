package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.base.individuals.Practitioner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.Test;


@SpringBootTest
@AutoConfigureMockMvc
public class PractitionerControllerTest{

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;
    
    
    @SneakyThrows
    @Test
    public void
    getAllPractitioner(){
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.get("/api/practitioner"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @SneakyThrows
    @Test
    public void
    getAPractitioner(){
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.get("/api/practitioner/23"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @SneakyThrows
    @Test
    public void
    postAPractitioner(){
        Practitioner practitioner =new Practitioner();
        String json= null;
        try {
            json = om.writeValueAsString(practitioner);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.post("/api/practitioner/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @SneakyThrows
    @Test
    public void
    putAPractitioner(){
        Practitioner practitioner=new Practitioner();
        String json= null;
        try {
            json = om.writeValueAsString(practitioner);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.put("/api/practitioner/23")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    @Test
    public void
    deleteAPractitioner(){
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/practitioner/23"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
