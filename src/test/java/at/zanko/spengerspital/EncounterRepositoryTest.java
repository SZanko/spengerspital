package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.base.management.Encounter;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.repositories.EncounterRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
public class EncounterRepositoryTest {

    @Autowired
    EncounterRepository encounterRepository;

    @Test
    public void testSaveOneEncounter(){
        //1. Erstellen einer Encountereninstanz
        Set<Identifier> identifier=new HashSet<>();
        Set<Coding> coding =new HashSet<>();
        Coding tempcoding=new Coding("System","0.1.1","Code","<div> ... </div>",true);
        coding.add(new Coding("System","0.1.1","Code","<div> ... </div>",false));
        CodeableConcept ccType=new CodeableConcept(coding,"Text");
        Period period =new Period(LocalDateTime.of(2001,01,01,1,1),LocalDateTime.of(2001,01,01,1,1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType,"System","value",period));
        Set<ContactPoint> phonenumbers=new HashSet<>();
        phonenumbers.add(new ContactPoint(ContactPoint.SystemCode.other,"Value",ContactPoint.UseCode.home,0,
                        new Period(LocalDateTime.of(2000,01,01,1,1),
                                LocalDateTime.of(2000,01,01,1,1))));
        Set<HumanName> humanname=new HashSet<>();
        Set<String> tempstring=new HashSet<>();
        tempstring.add("test");
        tempstring.add("test1");
        Set<String> given=tempstring;
        tempstring.add("prefix");
        Set<String> prefix=tempstring;
        tempstring.add("suffix");
        Set<String> suffix=tempstring;
        humanname.add(new HumanName(HumanName.UseCode.old,"temp","test",given,prefix,suffix,period));

        Encounter e =new Encounter(identifier, Encounter.StatusCode.onleave,tempcoding,period,"50min");
      //  //2. Instanz mit Testdaten befüllen
      //  //...
      //  //3. Instanz in die DB speichern
        Encounter savedE=encounterRepository.save(e);
      //  //4. Gespeicherte Daten aus der DB lesen
        Encounter loaded=encounterRepository.findById(savedE.getId()).get();
      //  //5. Vergleich der gespeicherten Daten mit den geladen
      //  assertTrue(p.isActive()==loaded.isActive());
      //  assertNotNull(loaded.getId());
      //  //assertTrue(CollectionUtils.isEqualCollection(p.getName(),loaded.getName()));
      //  assertEquals(p.getGender(),loaded.getGender());
    }
}
