package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.clinical.medications.Medication;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.model.type.resource.clinical.medications.Medication_Batch;
import at.zanko.spengerspital.model.type.resource.clinical.medications.Medication_Ingredient;
import at.zanko.spengerspital.repositories.MedicationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
public class MedicationRepositoryTest{

    @Autowired
    MedicationRepository medicationRepository;

    @Transactional
    @Test
    public void testSaveOneMedication() {
        //1. Erstellen einer Medicationeninstanz
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        Coding tempcoding = new Coding("System", "0.1.1", "Code", "<div> ... </div>", true);
        coding.add(new Coding("System", "0.1.1", "Code", "<div> ... </div>", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(LocalDateTime.of(2001, 01, 01, 1, 1), LocalDateTime.of(2001, 01, 01, 1, 1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period));
        coding.add(new Coding("System", "0.1.1", "Code", "<div> ... </div>", false));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period));
        Set<ContactPoint> phonenumbers = new HashSet<>();
        phonenumbers.add(new ContactPoint(ContactPoint.SystemCode.other, "Value", ContactPoint.UseCode.home, 0,
                new Period(LocalDateTime.of(2000, 01, 01, 1, 1),
                        LocalDateTime.of(2000, 01, 01, 1, 1))));
        Reference r1 = new Reference();
        Medication_Batch med_bat = new Medication_Batch("Test", LocalDateTime.of(2020, 5, 5, 5, 5));

        Quantity q1 = new Quantity(1.5f, Quantity.ComparatorCode.smaller, "testunit", "https://www.example.com");
        Ratio rat1 = new Ratio(q1,q1);
        Medication_Ingredient medin1 = new Medication_Ingredient(ccType, true, rat1);
        Set<Medication_Ingredient> med_in = new HashSet<>();
        med_in.add(medin1);
        Medication e = new Medication(identifier, ccType, Medication.StatusCode.inactive, r1, ccType, rat1, med_in, med_bat);
        //  //2. Instanz mit Testdaten befüllen
        //  //...
        //  //3. Instanz in die DB speichern
        Medication savedP=medicationRepository.save(e);
        //  //4. Gespeicherte Daten aus der DB lesen
        Medication loaded=medicationRepository.findById(savedP.getId()).get();
        //  //5. Vergleich der gespeicherten Daten mit den geladen
        //  assertTrue(p.isActive()==loaded.isActive());
        //  assertNotNull(loaded.getId());
        //  //assertTrue(CollectionUtils.isEqualCollection(p.getName(),loaded.getName()));
        //  assertEquals(p.getGender(),loaded.getGender());
    }
}
