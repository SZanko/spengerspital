package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.specialized.publichealth.ResearchSubject;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.repositories.ResearchSubjectRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@SpringBootTest
public class ResearchSubjectRepositoryTest {

    @Autowired
    ResearchSubjectRepository researchsubjectRepository;

    @Transactional
    @Test
    public void testSaveOneResearchSubject() {
        //1. Erstellen einer ResearchSubjecteninstanz
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        Coding tempcoding = new Coding("System", "0.1.1", "Code", "<div> ... </div>", true);
        coding.add(new Coding("System", "0.1.1", "Code", "<div> ... </div>", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(LocalDateTime.of(2001, 01, 01, 1, 1), LocalDateTime.of(2001, 01, 01, 1, 1));
        Identifier tmpidentifier = new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period);
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period));
        coding.add(new Coding("System", "0.1.1", "Code", "<div> ... </div>", false));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period));
        Set<ContactPoint> phonenumbers = new HashSet<>();
        phonenumbers.add(new ContactPoint(ContactPoint.SystemCode.other, "Value", ContactPoint.UseCode.home, 0,
                new Period(LocalDateTime.of(2000, 01, 01, 1, 1),
                        LocalDateTime.of(2000, 01, 01, 1, 1))));
        Reference r1 = new Reference("hanswurst", "kein type", tmpidentifier, "vielleicht" );

        Quantity q1 = new Quantity(1.5f, Quantity.ComparatorCode.smaller, "testunit", "https://www.example.com");
        Ratio rat1 = new Ratio(q1, q1);
        ResearchSubject researchSubject = new ResearchSubject(tmpidentifier, ResearchSubject.StatiCode.inactive, period, r1 , LocalDateTime.of(2001,01,01,01,01));
        System.out.println(researchSubject.toString());
        //  //2. Instanz mit Testdaten befüllen
        //  //...
        //  //3. Instanz in die DB speichern
        ResearchSubject savedP = researchsubjectRepository.save(researchSubject);
        //  //4. Gespeicherte Daten aus der DB lesen
        // ResearchSubject loaded = researchsubjectRepository.findById(savedP.getId()).get();
        //  //5. Vergleich der gespeicherten Daten mit den geladen
        // assertTrue(savedP.getDatetime()==loaded.getDatetime());
        //  assertNotNull(loaded.getId());
        //  //assertTrue(CollectionUtils.isEqualCollection(p.getName(),loaded.getName()));
        //  assertEquals(p.getGender(),loaded.getGender());
    }
}
