package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.base.individuals.Practitioner;
import at.zanko.spengerspital.model.type.resource.base.individuals.Practitioner_Qualification;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.repositories.PractitionerRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.MimeType;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PractitionerRepositoryTest{
    @Autowired
    PractitionerRepository practitionerRepository;

    @Test
    public void testSaveOnePractitioner(){
        Set<Identifier> identifier=new HashSet<>();
        Set<Coding> coding =new HashSet<>();
        coding.add(new Coding("System","0.1.1","Code","<div> ... </div>",false));
        CodeableConcept ccType=new CodeableConcept(coding,"Text");
        Period period =new Period(LocalDateTime.of(2001,01,01,1,1),LocalDateTime.of(2001,01,01,1,1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType,"System","value",period));

        Set<HumanName> name=new HashSet<>();
        Set<String> hnamstring=new HashSet<>();
        hnamstring.add("Bla Bla");
        HumanName hanswurst=new HumanName(HumanName.UseCode.temp,"humannametext","humanfamily",hnamstring,hnamstring,hnamstring,period);
        name.add(hanswurst);
        Set<ContactPoint> telecom=new HashSet<>();
        telecom.add(new ContactPoint(ContactPoint.SystemCode.other,"Value",ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));
        Set<Address> address=new HashSet<>();
        LocalDate birthDate=LocalDate.now();
        Set<Attachment> photo=new HashSet<>();
        try {
            Attachment tmpat=new Attachment(MimeType.valueOf("image/png"), Attachment.LanguageCode.de,"Daten",new URL("https://example.com"),63234,"TestHash","Prof",LocalDateTime.now());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Set<Practitioner_Qualification> practitionerQualification =new HashSet<>();
        practitionerQualification.add(new Practitioner_Qualification(identifier,ccType,period));
        Set<CodeableConcept> communication=new HashSet<>();
        communication.add(ccType);
        Practitioner p=new Practitioner(identifier,true,name,telecom,address,Practitioner.GenderCode.other,birthDate,photo, practitionerQualification,communication);

        Practitioner savedP=practitionerRepository.save(p);
        Practitioner loaded=practitionerRepository.findById(savedP.getId()).get();
        assertEquals(loaded.isActive(), savedP.isActive());
        assertNotNull(loaded.getId());
        assertTrue(CollectionUtils.isEqualCollection(savedP.getName(),loaded.getName()));
        assertEquals(savedP.getId(),loaded.getId());
        assertEquals(savedP.getAddress(),loaded.getAddress());
        assertEquals(savedP.getGender(),loaded.getGender());
        assertTrue(CollectionUtils.isEqualCollection(savedP.getName(),loaded.getName()));
        assertTrue(CollectionUtils.isEqualCollection(savedP.getTelecom(),loaded.getTelecom()));
        assertEquals(savedP.getPhoto(),loaded.getPhoto());
        // assertTrue(CollectionUtils.isEqualCollection(savedP.getCommunication(),loaded.getCommunication()));
        // assertTrue(CollectionUtils.isEqualCollection(savedP.getPractitionerQualification(),loaded.getPractitionerQualification()));
        assertEquals(savedP.getBirthDate(),loaded.getBirthDate());
        CollectionUtils.isEqualCollection(savedP.getIdentifier(),loaded.getIdentifier());
        //savedP.getIdentifier().removeAll(loaded.getIdentifier());
        assertEquals(savedP.getIdentifier().toString(),loaded.getIdentifier().toString());
    }
}
