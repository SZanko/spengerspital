package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.base.individuals.PractitionerRole;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.model.type.resource.base.individuals.Practitioner_Role_availableTime;
import at.zanko.spengerspital.repositories.PractitionerRoleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@SpringBootTest
public class PractitionerRoleRepositoryTest {


    @Autowired
    PractitionerRoleRepository practitionerroleRepository;

    @Transactional
    @Test
    public void testSaveOnePractitionerRole() {
        //1. Erstellen einer PractitionerRole  instanz
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        Coding tempcoding = new Coding("System", "0.1.1", "Code", "<div> ... </div>", true);
        coding.add(new Coding("System", "0.1.1", "Code", "<div> ... </div>", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(LocalDateTime.of(2001, 01, 01, 1, 1), LocalDateTime.of(2001, 01, 01, 1, 1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period));
        coding.add(new Coding("System", "0.1.1", "Code", "<div> ... </div>", false));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType, "System", "value", period));
        Set<CodeableConcept> ccset = new HashSet<>();
        ccset.add(ccType);
        Set<ContactPoint> phonenumbers = new HashSet<>();
        phonenumbers.add(new ContactPoint(ContactPoint.SystemCode.other, "Value", ContactPoint.UseCode.home, 0,
                new Period(LocalDateTime.of(2000, 01, 01, 1, 1),
                        LocalDateTime.of(2000, 01, 01, 1, 1))));
        Reference r1 = new Reference();
        Reference r2 = new Reference();

        Set<Reference> refset = new HashSet<>();
        refset.add(r1);
        refset.add(r2);

        Quantity q1 = new Quantity(1.5f, Quantity.ComparatorCode.smaller, "testunit", "https://www.example.com");

        Ratio rat1 = new Ratio(q1, q1);

        Set<Practitioner_Role_availableTime> practitioner_role_availableTimeSet = new HashSet<>();
        Set<Practitioner_Role_availableTime.DaysOfWeek> daysOfWeeksSet = new HashSet<>();
        daysOfWeeksSet.add(Practitioner_Role_availableTime.DaysOfWeek.tue);
        Practitioner_Role_availableTime practitioner_role_availableTime = new Practitioner_Role_availableTime(daysOfWeeksSet, true, LocalDateTime.of(2001, 01, 01, 1, 1), LocalDateTime.of(2001, 01, 01, 1, 1));
        practitioner_role_availableTimeSet.add(practitioner_role_availableTime);

        PractitionerRole p = new PractitionerRole(identifier, true, period, r1, r2, ccset, ccset, refset, refset, phonenumbers, practitioner_role_availableTimeSet);

        //  //2. Instanz mit Testdaten befüllen
        //  //...
        //  //3. Instanz in die DB speichern
        PractitionerRole savedP = practitionerroleRepository.save(p);
        //  //4. Gespeicherte Daten aus der DB lesen
        PractitionerRole loaded = practitionerroleRepository.findById(savedP.getId()).get();
        //  //5. Vergleich der gespeicherten Daten mit den geladen
        assertTrue(p.isActive()==loaded.isActive());
        //  assertNotNull(loaded.getId());
        //  //assertTrue(CollectionUtils.isEqualCollection(p.getName(),loaded.getName()));
        //  assertEquals(p.getGender(),loaded.getGender());
    }
}
