package at.zanko.spengerspital;

import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.repositories.FlagRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class FlagRepositoryTest {

    @Autowired
    FlagRepository flagRepository;

    @Transactional
    @Test
    public void testSaveOneFlag(){
        Set<Identifier> identifier=new HashSet<>();
        Set<Coding> coding =new HashSet<>();
        coding.add(new Coding("System","0.1.1","Code","<div> ... </div>",false));
        CodeableConcept ccType=new CodeableConcept(coding,"Text");
        Period period =new Period(LocalDateTime.of(2001,01,01,1,1),LocalDateTime.of(2001,01,01,1,1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType,"System","value",period));
        Identifier tmpidentif= new Identifier(Identifier.UseCode.temp,ccType,"System","wert",period);
        Reference ref = new Reference("230","Patient",tmpidentif,"alternativer Text");

        Flag f =new Flag(identifier, Flag.FlagStatus.activity,period,ref);

        Flag saveF = flagRepository.save(f);
        Flag loadedF=flagRepository.findById(saveF.getId()).get();
        assertEquals(loadedF.getId(),saveF.getId());
        assertNotNull(loadedF.getId());

        assertEquals(saveF.getAuthor(),loadedF.getAuthor());
    }
}
