package at.zanko.spengerspital;

import at.zanko.spengerspital.model.type.data.*;
import org.apache.commons.collections4.CollectionUtils;
import at.zanko.spengerspital.repositories.DeviceUseStatementRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * <a href = https://www.hl7.org/fhir/deviceusestatement-definitions.html#DeviceUseStatement> Device Use Statment</a>
 *
 * @author	Stefan Zanko
 * @version 1.0
 * @since	2020-11-11
 */
@SpringBootTest
public class DeviceUseStatementRepositoryTest {

    @Autowired
    DeviceUseStatementRepository deviceUseStatementRepository;

    @Test
    public void testSaveOnDeviceUseStatement() {

        Set<Identifier> identifier=new HashSet<>();
        Set<Reference> references=new HashSet<>();
        Period period =new Period(LocalDateTime.of(2001,01,01,1,1),LocalDateTime.of(2001,01,01,1,1));
        DeviceUseStatement_Timing timing=new DeviceUseStatement_Timing(period,LocalDateTime.of(2001,01,01,1,1));
        LocalDateTime recorded=LocalDateTime.of(2001,01,01,23,4);

        DeviceUseStatement d = new DeviceUseStatement(identifier,references, DeviceUseStatement.DeviceUseStatement_Status.active,timing,recorded);
        DeviceUseStatement savedD=deviceUseStatementRepository.save(d);
        DeviceUseStatement loadedD=deviceUseStatementRepository.findById(savedD.getId()).get();
        assertNotNull(loadedD.getId());
        assertEquals(loadedD.getId(),savedD.getId());
        assertEquals(loadedD.getDeviceUseStatement_status(),savedD.getDeviceUseStatement_status());
        assertTrue(CollectionUtils.isEqualCollection(savedD.getIdentifier(), loadedD.getIdentifier()));
        assertTrue(CollectionUtils.isEqualCollection(savedD.getBasedOn(),loadedD.getBasedOn()));

        assertEquals(loadedD.getRecordedOn(),savedD.getRecordedOn());
    }
}
