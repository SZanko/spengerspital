package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.base.individuals.Patient;
import at.zanko.spengerspital.model.type.data.*;
import at.zanko.spengerspital.repositories.PatientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PatientRepositoryTest {

    @Autowired
    PatientRepository patientRepository;

    @Test
    public void testSaveOnePatient(){
        //1. Erstellen einer Patienteninstanz
        Set<Identifier> identifier=new HashSet<>();
        Set<Coding> coding =new HashSet<>();
        coding.add(new Coding("System","0.1.1","Code","<div> ... </div>",false));
        CodeableConcept ccType=new CodeableConcept(coding,"Text");
        Period period =new Period(LocalDateTime.of(2001,01,01,1,1),LocalDateTime.of(2001,01,01,1,1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType,"System","value",period));
        Set<ContactPoint> phonenumbers=new HashSet<>();
        phonenumbers.add(new ContactPoint(ContactPoint.SystemCode.other,"Value",ContactPoint.UseCode.home,0,
                        new Period(LocalDateTime.of(2000,01,01,1,1),
                                LocalDateTime.of(2000,01,01,1,1))));
        Set<HumanName> humanname=new HashSet<>();
        Set<String> hnamstring=new HashSet<>();
        hnamstring.add("Bla Bla");
        HumanName hanswurst=new HumanName(HumanName.UseCode.temp,"humannametext","humanfamily",hnamstring,hnamstring,hnamstring,period);
        humanname.add(hanswurst);

        Patient p =new Patient(true, Patient.GenderCode.female, LocalDate.of(2001,01,01),identifier,humanname,phonenumbers);
        //2. Instanz mit Testdaten befüllen
        //...
        //3. Instanz in die DB speichern
        Patient savedP=patientRepository.save(p);
        //4. Gespeicherte Daten aus der DB lesen
        Patient loaded=patientRepository.findById(savedP.getId()).get();
        //5. Vergleich der gespeicherten Daten mit den geladen
        assertTrue(p.isActive()==loaded.isActive());
        assertNotNull(loaded.getId());
        //assertTrue(CollectionUtils.isEqualCollection(p.getName(),loaded.getName()));
        assertEquals(p.getGender(),loaded.getGender());
    }
}
