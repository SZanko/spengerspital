package at.zanko.spengerspital;

import at.zanko.spengerspital.model.resource.base.individuals.Patient;
import at.zanko.spengerspital.model.type.data.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@SpringBootTest
@AutoConfigureMockMvc
public class PatientControllerTest{
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @Test
    public void
    getAllPatients(){
        try {
            mockMvc
            .perform(MockMvcRequestBuilders.get("/api/patient"))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void
    getAPatient(){
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.get("/api/patient/Test"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void
    postAPatient(){

        Set<Identifier> identifier=new HashSet<>();
        Set<Coding> coding =new HashSet<>();
        coding.add(new Coding("System","0.1.1","Code","<div> ... </div>",false));
        CodeableConcept ccType=new CodeableConcept(coding,"Text");
        Period period =new Period(LocalDateTime.of(2001,01,01,1,1),LocalDateTime.of(2001,01,01,1,1));
        identifier.add(new Identifier(Identifier.UseCode.offical, ccType,"System","value",period));
        Set<ContactPoint> phonenumbers=new HashSet<>();
        phonenumbers.add(new ContactPoint(ContactPoint.SystemCode.other,"Value",ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));
        Set<HumanName> humanname=null;

        Patient patient =new Patient(true, Patient.GenderCode.female, LocalDate.of(2001,01,01),identifier,humanname,phonenumbers);

        String json= null;
        try {
            json = om.writeValueAsString(patient);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.post("/api/patient/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void
    putAPatient(){
        Patient patient=new Patient();
        String json= null;
        try {
            json = om.writeValueAsString(patient);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.put("/api/patient/Test")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    @Test
    public void
    deleteAPatient(){
        try {
            mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/patient/Test"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
