mkdir -p spengerspital-szanko
# Weil Windows Powershell COPY-ITEM es nicht schafft mehrere Files auf einmal zu kopieren schreibe ich jetzt für jede einzelne Zeile copy
# cp -R LICENSE Makefile README.md config.mk src/ spengerspital-zanko
cp LICENSE spengerspital-szanko
cp Makefile spengerspital-szanko
cp README.md spengerspital-szanko
cp config.mk spengerspital-szanko
cp -R src/ spengerspital-szanko
Compress-Archive -Path spengerspital-szanko -DestinationPath spengerspital-szanko.zip
